import { APIGatewayProxyEventV2, Context } from 'aws-lambda';

// event and context fixtures for use in tests
export const event = {
  version: '2.0',
  routeKey: '$default',
  rawPath: '/',
  rawQueryString: '',
  headers: {
    accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.5',
    'content-length': '0',
    host: 'xyzabcxyzj.execute-api.eu-west-1.amazonaws.com',
    'sec-fetch-dest': 'document',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-site': 'cross-site',
    'sec-fetch-user': '?1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:98.0) Gecko/20100101 Firefox/98.0',
    'x-amzn-trace-id': 'Root=1-62363ca3-4f531e9c3182e66858af1dd8',
    'x-forwarded-for': '255.255.255.255',
    'x-forwarded-port': '443',
    'x-forwarded-proto': 'https'
  },
  requestContext: {
    accountId: '123456789012',
    apiId: 'xyzabcxyzj',
    domainName: 'xyzabcxyzj.execute-api.eu-west-1.amazonaws.com',
    domainPrefix: 'xyzabcxyzj',
    http: {
      method: 'GET',
      path: '/',
      protocol: 'HTTP/1.1',
      sourceIp: '255.255.255.255',
      userAgent: 'Mozilla/5.0 (X11; Linux x86_64; rv:98.0) Gecko/20100101 Firefox/98.0'
    },
    requestId: 'PP5ppimqDoEEJBw=',
    routeKey: '$default',
    stage: '$default',
    time: '19/Mar/2022:20:27:15 +0000',
    timeEpoch: 1647721635961
  },
  isBase64Encoded: false
} as APIGatewayProxyEventV2;

export const context = {} as Context;
