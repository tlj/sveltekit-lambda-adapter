import * as fs from 'fs';

import { Handler } from '../../sveltekit-lambda-adapter/svelte';
import { event, context } from '../fixtures/lambda-fixtures';
import { Server } from '../../lambda/index';
import { Headers, Request } from 'node-fetch'

jest.mock('fs');
// jest.mock('../../lambda/index')
beforeEach(() => {
    event.rawPath = '/';
});

describe('Unit tests for handler', () => {
    const err = jest.spyOn(console, 'error').mockImplementation(() => { });
    const inf = jest.spyOn(console, 'info').mockImplementation(() => { });

    it('fetchLocal should return a b64 encoded payload for a local file', async () => {
        const file = jest.spyOn(fs, 'existsSync').mockImplementation(() => {
            return true;
        });
        const spy = jest.spyOn(fs, 'readFileSync').mockImplementation(() => {
            return 'b64string';
        });
        const testHandler = new Handler(event, context);
        const result = testHandler.fetchLocal('favicon.png');
        // const result = await handler(event, context);

        expect(spy).toBeCalledWith('favicon.png', { encoding: 'base64' });
        expect(result.statusCode).toEqual(200);
        expect(result.isBase64Encoded).toBe(true);
        expect(result.headers).toEqual({
            'Content-Type': 'image/png',
        })
    });

    it('fetchLocal should return 404 if theres no local file', async () => {
        const file = jest.spyOn(fs, 'existsSync').mockImplementation(() => {
            return false;
        });
        const testHandler = new Handler(event, context);
        const result = testHandler.fetchLocal('missing-file.png');
        expect(result.statusCode).toEqual(404);
    });

    it('execute should call fetchLocal if the rawPath is a file', async () => {
        event.rawPath = '/favicon.png';
        const testHandler = new Handler(event, context);
        const spy = jest.spyOn(testHandler, 'fetchLocal');
        testHandler.execute(event);
        expect(spy).toBeCalledWith('favicon.png');
    });

    it('execute should not call fetchLocal if the rawPath is not a file', async () => {
        event.rawPath = '/about';
        const mockFetchLocal = jest
            .spyOn(Handler.prototype, 'fetchLocal');
        const testHandler = new Handler(event, context);
        testHandler.execute(event);
        expect(mockFetchLocal).not.toHaveBeenCalled();
    });

    it('responseBuilder should return an object with a body, headers and a statusCode', async () => {
        const testHandler = new Handler(event, context);
        const body = 'Run and tell all of the angels';
        const headers = { 'Content-Type': 'music/foo' };
        const result = testHandler.responseBuilder(
            body,
            headers,
            200,
        );
        expect(result).toEqual({ body: JSON.stringify(body), headers, statusCode: 200 });
    });

    it('handler should log and return 500 if an error occurs', async () => {
        const mockExecuteHandler = jest
            .spyOn(Handler.prototype, 'execute')
            .mockImplementation(() => {
                throw new Error('Make my way back home when I learn to fly.');
            });
        const testHandler = Handler.handler(event, context);
        expect(mockExecuteHandler).toHaveBeenCalled();
        expect(err).toBeCalledWith(Error('Make my way back home when I learn to fly.'));
        mockExecuteHandler.mockRestore();
    });

    it('execute should return a valid body and status 200 on a correct path', async () => {
        const text = async () => { return 'html' }
        const mockServer = jest
            .spyOn(Server.prototype, "respond")
            .mockImplementation(async () => { return { text, status: 200 } })

        const testHandler = new Handler(event, context)
        const result = await testHandler.execute(event)
        const expectedRequest = new Request('https://xyzabcxyzj.execute-api.eu-west-1.amazonaws.com/', {
            method: 'GET',
            headers: new Headers(event.headers as unknown as Headers)
        })
        expect(mockServer).toBeCalledWith(expectedRequest)
        expect(result.body).toContain('html')
        expect(result.statusCode).toBe(200)
    })

});
