## SvelteKit Lambda Adapter

An adapter that makes as few assumptions as possible about what infrastructure you might like to use within AWS. This means all files; client, server and static are stored on a single lambda which in my case is proxying a http api gateway, with catch all routing.

I've tested this with a node lambda that is using the minimum memory possible (128mb) and even with coldstarts most content loads within 1s, with the opportunity to improve dramatically afterwards.

There's probably lot's of reasons you wouldn't want to do this, but it suits my needs so here we are.

## Installation

`npm install -D @djverrall/sveltekit-lambda-adapter`

You will then need to alter your `svelte.config.js` to use the adapter:

```
import adapter from '@djverrall/sveltekit-lambda-adapter';
import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess(),

	kit: {
		adapter: adapter()
	}
};

export default config;
```


## Usage

Once installed and adjusted in your svelte config, you may run `npm run build` which will generate a `.aws-svelte/` folder, along with a zip of the built code which can be directly loaded into lambda. The default handler path at the moment is `svelte.handler`. 

How you deploy the lambda (and assosciated API gateway most likely) is up to you, I use both pulumi and terraform. If you want to use SAM/Serverless or others you can point to the `.aws-svelte/lambda` folder, keeping in mind the handler path. 

## Support

Right now this is highly experimental and unsupported. Please use at your own risk and consider that this may not be the solution for production systems. There are much better products that are better developed than this. I'd never worked with JS or TS really a fortnight ago, so expect this code to be of fairly low quality.

## Project Status
I've created this package so I can easily reuse the code in my own projects. I don't particularly want or need the overhead of cloudfront distros, s3 or other infrastructure, so this is just the shortest cheapest path to serving my sveltekit projects using AWS (for me). Please bear it in mind that I may never contribute to this again - and as a consequence if something isn't working for you - I probably will not help. That all said, thanks for stopping by.