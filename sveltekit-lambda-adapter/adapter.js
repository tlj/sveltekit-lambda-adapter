import esbuild from 'esbuild';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/** @type {import('@sveltejs/kit').AdapterSpecificOptions} */
/** @param {AdapterSpecificOptions} options */
export default function (options) {
    /** @type {import('@sveltejs/kit').Adapter} */

    const build = '.aws-svelte';
    const handler = 'svelte';

    const adapter = {
        name: 'lambda-sveltekit',
        async adapt(builder) {
            builder.log.minor('Clearing build dir and writing assets.')

            builder.rimraf(build);

            builder.log.minor('Compiling Sveltekit.')

            builder.writeServer(`${build}/lambda`);
            builder.writeStatic(`${build}/lambda`);
            builder.writeClient(`${build}/lambda`)

            builder.log.minor('Building lambda handler.')
            builder.copy(`${__dirname}/_svelte.ts`, `${build}/lambda/_svelte.ts`)
            builder.copy(`${__dirname}/shims.js`, `${build}/lambda/shims.js`)

            esbuild.buildSync({
                entryPoints: [`${build}/lambda/_svelte.ts`],
                outfile: `${build}/lambda/${handler}.js`,
                inject: [`${build}/lambda/shims.js`],
                external: ['node:*'],
                bundle: true,
                minify: true,
                format: 'cjs',
                platform: 'node',
            })

            builder.rimraf(`${build}/lambda/_svelte.ts`)

            builder.log.success(`Lambda built, your handler path is ${handler}.handler`)

        }
    };

    return adapter;
}
