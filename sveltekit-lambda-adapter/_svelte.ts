import * as fs from 'fs';
import * as path from 'path';
import { Buffer } from 'buffer';

import type { Context, APIGatewayProxyEventV2, APIGatewayProxyResultV2, APIGatewayEventRequestContextV2, APIGatewayProxyEventHeaders } from 'aws-lambda';
import { contentType } from 'mime-types';
import { Headers, Request, Response } from 'node-fetch';

import { Server } from './index.js';
import { manifest } from './manifest.js';

export class Handler {
  static handler(this: void, event: APIGatewayProxyEventV2, context: Context) {
    const handler = new Handler(event, context);
    try {
      /* istanbul ignore next */
      return handler.execute(event);
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        body: JSON.stringify('Lambda execution failed.'),
      };
    }
  }

  event: APIGatewayProxyEventV2;
  context: Context;

  constructor(event: APIGatewayProxyEventV2, context: Context) {
    this.event = event;
    this.context = context;
    console.info('Event received', event);
  }


  responseBuilder(body: string, headers: any, status: number): APIGatewayProxyResultV2 {
    return {
      statusCode: status,
      headers,
      body: JSON.stringify(body),
    };
  }

  async execute(event: APIGatewayProxyEventV2) {
    const { headers, body, rawPath, requestContext, isBase64Encoded } = event;
    const cleanPath: string = rawPath.substring(1);
    const fullPath = `https://${requestContext.domainName + rawPath}`;

    const encoding: BufferEncoding = isBase64Encoded ? 'base64' : headers['content-encoding'] as any || 'utf-8';
    const parsedBody = typeof body === 'string' ? Buffer.from(body, encoding) : body;

    const app = new Server(manifest);

    if (path.extname(cleanPath)) {
      if (fs.existsSync(cleanPath)) {
        const res = this.fetchLocal(cleanPath);
        return res;
      }

      return this.render(fullPath, app, requestContext, headers, parsedBody as any)
    }
    
    return this.render(fullPath, app, requestContext, headers, parsedBody as any)
  }

  async render(
    uri: string,
    app: Server,
    requestContext: APIGatewayEventRequestContextV2,
    headers: APIGatewayProxyEventHeaders,
    body: string) {
    try {
      const rendered: Response = await app.respond(new Request(uri, {
        method: requestContext.http.method,
        headers: new Headers(headers as unknown as Headers),
        body: body,
      }));

      const resHeaders: any = {}
      for (let k of rendered.headers.keys()) {
        const v: any = rendered.headers.get(k)

        if (v instanceof Array) {
          resHeaders[k] = v
        } else {
          resHeaders[k] = v
        }
      }

      const response: APIGatewayProxyResultV2 = {
        body: await rendered.text(),
        headers: resHeaders,
        statusCode: rendered.status,
      };
      return response;
    } catch (error) {
      console.error(error);
      return {
        body: `A fatal error occured. ${error}`,
        statusCode: 500,
      };
    }
  }

  fetchLocal(uri: string) {
    const data: string = fs.readFileSync(uri, { encoding: 'base64' });

    const contentHeader = contentType(path.extname(uri));

    const res: APIGatewayProxyResultV2 = {
      headers: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        'Content-Type': contentHeader,
      },
      statusCode: 200,
      body: data,
      isBase64Encoded: true,
    };

    return res;
  }

}

export const handler = Handler.handler;
