module.exports = {
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: [
    "/lambda/",
    "/tests/"
  ],
  coverageProvider: "babel",
  testEnvironment: "node",
  testMatch: [
    "**/unit/**/*.test.ts"
  ],
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest",
    "^.+\\.(js)$": "babel-jest",
  },
  transformIgnorePatterns: [
  ],
};
